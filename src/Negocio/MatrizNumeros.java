/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author madarme
 */
public class MatrizNumeros {

    private ListaNumeros[] filas;

    public MatrizNumeros() {
    }

    /**
     * Constructor con procesamiento de cadenas del tipo: elemento, .... ;
     * elemento, .... ; ....
     *
     * @param cadena un String con la información de la matriz
     */
    public MatrizNumeros(String cadena) {
        if (cadena.isEmpty()) {
            throw new RuntimeException("Es imposible crear la matriz, no hay datos");
        }

        String filasDatos[] = cadena.split(";");
        //Crear el vector de filas:
        this.filas = new ListaNumeros[filasDatos.length];
        //Debo crear las columas para cada fila
        for (int i = 0; i < filasDatos.length; i++) {
            String columnasDato[] = filasDatos[i].split(",");
            //Crear el vector de float:
            ListaNumeros columna = new ListaNumeros(columnasDato.length);
            this.pasarDatos_Columna(columna, columnasDato);
            //Ingresar la columna a la Matriz:
            this.filas[i] = columna;

        }

    }

    private void pasarDatos_Columna(ListaNumeros columna, String datos[]) {
        for (int i = 0; i < datos.length; i++) {
            columna.adicionar(i, Float.parseFloat(datos[i]));

        }
    }

    public MatrizNumeros(int cantFilas) {

        this.filas = new ListaNumeros[cantFilas];
    }

    public ListaNumeros[] getFilas() {
        return filas;
    }

    public void setFilas(ListaNumeros[] filas) {
        this.filas = filas;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.filas.length) {
            throw new RuntimeException("Índice de la fila fuera de rango:" + i);
        }
    }

    public ListaNumeros getElementoVector(int i) {

        this.validar(i);
        return this.filas[i];
    }

    public void adicionar(int i, ListaNumeros listaNueva) {
        this.validar(i);
        this.filas[i] = listaNueva;
    }

    @Override
    public String toString() {
        String msg = "";
        for (ListaNumeros lista : this.filas) {
            msg += lista.toString() + "\n";

        }
        return msg;
    }

    /**
     * Retorna la cantidad de filas
     *
     * @return un entro con la cantidad de filas
     */
    public int length() {
        return this.filas.length;
    }

    /**
     * Retorna el tipo de matriz: Cuadrada, rectangular o dispersa
     *
     * @return una cadena con el tipo de matriz
     */
    public String getTipo() {
        return null;
    }
}
