/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *  Esto representa una persona con su fecha de nacimiento
 * @author madarme
 */
public class Persona implements Comparable{
    
    private int cedula;
    //Fecha de nacimiento
    private short agno,mes,dia,h,m,s;
    private String nombresCompletos;

    public Persona() {
    }

    
    /**
     * 
     * @param cedula
     * @param agno
     * @param mes
     * @param dia
     * @param h
     * @param m
     * @param s
     * @param nombresCompletos 
     */
    public Persona(int cedula, short agno, short mes, short dia, short h, short m, short s, String nombresCompletos) {
        this.cedula = cedula;
        this.agno = agno;
        this.mes = mes;
        this.dia = dia;
        this.h = h;
        this.m = m;
        this.s = s;
        this.nombresCompletos = nombresCompletos;
    }

    /*
        validar si la fecha de nacimiento existe, usando excepciones
    */
    
    
    
    
    
    @Override
    public int compareTo(Object o) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
