package Vistas;

import Negocio.ListaNumeros;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class TestVectores {

    public static void main(String[] args) {

        ListaNumeros lista = new ListaNumeros(leerEntero("Digite cantidad de elementos: "));
        int x;
        do {
            x = menu();
            switch (x) {
                case 1:
                    adicionar(lista);
                    break;
                case 2:
                    mostrarVector(lista);
                    break;
                case 3:
                    actualizar(lista);
                    break;
                case 4:
                    eliminar(lista);
                    break;
                case 5:
                    ordenarBurbuja(lista);
                    break;
                case 6:
                    ordenar_Seleccion(lista);
                    break;
            }
        } while (x != 7);
    }

    private static float leerFloat(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextFloat();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerFloat(msg);
        }

    }

    private static int leerEntero(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un entero");
            return leerEntero(msg);
        }

    }

    private static int menu() {
        int x;
        Scanner entrada = new Scanner(System.in);
        System.out.println("Menu\n1. Adicionar datos al vector\n2. Mostrar\n3. Actualizar\n4. Eliminar posición y"
                + " redimensionar vector\n5. Ordenar por el método de la burbuja\n6. Ordenar por el método de selección\n7. salir");
        x = entrada.nextInt();
        return x;
    }

    private static void adicionar(ListaNumeros lista) {
        /**
         * Como vamos a realizar un proceso de almacenamiento usamos el for
         * convencional
         */
        for (int i = 0; i < lista.length(); i++) {
            lista.adicionar(i, leerFloat("Digite dato en la posición [" + i + "]:"));
        }
    }

    private static void mostrarVector(ListaNumeros lista) {

        System.out.println("Su lista es:" + lista.toString());
    }

    private static void actualizar(ListaNumeros lista) {
        for (int i = 0; i < lista.length(); i++) {
            lista.actualizar(i, leerFloat("Actualize su dato en la posición [" + i + "]:"));
        }
        System.out.println("Su lista a sido actualizada con exito.");
    }

    private static void eliminar(ListaNumeros lista) {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Posicion vector");
        int posicion = entrada.nextInt();
        if (lista.eliminar(posicion)) {
            System.out.println("Eliminado correctamente");
        } else {
            System.out.println("No existe dicha posicion");
        }

    }

    private static void ordenarBurbuja(ListaNumeros lista) {
        for (int i = 0; i < lista.length(); i++) {
            if (lista.ordenarBurbuja()) {
                System.out.println("Metodo realizado exitosamente, verifique su vector.");
            } else {
                System.out.println("vector vacio");
            }

        }
    }

    private static void ordenar_Seleccion(ListaNumeros lista) {
        for (int i = 0; i < lista.length(); i++) {
            lista.ordenar_Seleccion();
        }
        for (int i = (1 - lista.length()); i >= 0; i--) {
            lista.ordenar_Seleccion();

        }
    }
}
